
/*
 * Florigami Productivity Tools
 */
FloTools = {
    version         : '2015-12-09',
    placeId         : 'body',
    containerId     : '#florigami-productivity-tools',
    getParams       : {},
    q               : "",
    type            : undefined,
    state : {
        selectedRowIndex : undefined
    },
    timer           : 0,
    DOM             : {},
    fn              : {}
};


/*****************************
 *      Helper functions     *
 *****************************/

FloTools_Utilities = {};

/**
 * Retourne les paramètres GET de l'URL
 * @return Object
 */
FloTools_Utilities.extractGetParams = function() {    
    return FloTools_Utilities.deserialize( window.location.search.substr(1) );
};

/**
 * Transforme une chaine de type param1=value1&param2=value2... 
 * sous forme d'un objet littéral { 'param1' : 'value1', 'param2' : 'value2' }
 * @param {String} str
 * @return {Object}
 */
FloTools_Utilities.deserialize = function( str ) {

    var _object  = {},
        _couples = str.split("&"),
        _keyValue;
    
    for( var i = 0; i < _couples.length; i++ ) {
        _keyValue = _couples[i].split("=");
        _object[ decodeURIComponent( _keyValue[0] ) ] = ( _keyValue.length > 1 ) ? decodeURIComponent( _keyValue[1] ) : "";
    }
    
    return _object;

};

// http://stackoverflow.com/a/7394787
function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

// http://css-tricks.com/snippets/javascript/strip-html-tags-in-javascript/
function strip_tags( txt ) {
    return txt.replace(/(<([^>]+)>)/ig,"");
}

function removeDiacritics (str) {

  var defaultDiacriticsRemovalMap = [
    {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
    {'base':'AA','letters':/[\uA732]/g},
    {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
    {'base':'AO','letters':/[\uA734]/g},
    {'base':'AU','letters':/[\uA736]/g},
    {'base':'AV','letters':/[\uA738\uA73A]/g},
    {'base':'AY','letters':/[\uA73C]/g},
    {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
    {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
    {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
    {'base':'DZ','letters':/[\u01F1\u01C4]/g},
    {'base':'Dz','letters':/[\u01F2\u01C5]/g},
    {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
    {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
    {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
    {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
    {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
    {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
    {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
    {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
    {'base':'LJ','letters':/[\u01C7]/g},
    {'base':'Lj','letters':/[\u01C8]/g},
    {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
    {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
    {'base':'NJ','letters':/[\u01CA]/g},
    {'base':'Nj','letters':/[\u01CB]/g},
    {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
    {'base':'OI','letters':/[\u01A2]/g},
    {'base':'OO','letters':/[\uA74E]/g},
    {'base':'OU','letters':/[\u0222]/g},
    {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
    {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
    {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
    {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
    {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
    {'base':'TZ','letters':/[\uA728]/g},
    {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
    {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
    {'base':'VY','letters':/[\uA760]/g},
    {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
    {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
    {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
    {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
    {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
    {'base':'aa','letters':/[\uA733]/g},
    {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
    {'base':'ao','letters':/[\uA735]/g},
    {'base':'au','letters':/[\uA737]/g},
    {'base':'av','letters':/[\uA739\uA73B]/g},
    {'base':'ay','letters':/[\uA73D]/g},
    {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
    {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
    {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
    {'base':'dz','letters':/[\u01F3\u01C6]/g},
    {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
    {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
    {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
    {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
    {'base':'hv','letters':/[\u0195]/g},
    {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
    {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
    {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
    {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
    {'base':'lj','letters':/[\u01C9]/g},
    {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
    {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
    {'base':'nj','letters':/[\u01CC]/g},
    {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
    {'base':'oi','letters':/[\u01A3]/g},
    {'base':'ou','letters':/[\u0223]/g},
    {'base':'oo','letters':/[\uA74F]/g},
    {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
    {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
    {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
    {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
    {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
    {'base':'tz','letters':/[\uA729]/g},
    {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
    {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
    {'base':'vy','letters':/[\uA761]/g},
    {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
    {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
    {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
    {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
  ];

  for(var i=0; i<defaultDiacriticsRemovalMap.length; i++) {
    str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
  }

  return str;

}

/*!
 * Sheetrock v1.0.0
 * Quickly connect to, query, and lazy-load data from Google Sheets.
 * http://chriszarate.github.io/sheetrock/
 * License: MIT
 */
(function(e,r,t){"use strict";if(typeof define==="function"&&define.amd){define("sheetrock",function(){return t(null,r)})}else if(typeof module==="object"&&module.exports){module.exports=t(require("request"),global||r)}else{r[e]=t(null,r)}})("sheetrock",this,function(e,r){"use strict";var t={2014:{apiEndpoint:"https://docs.google.com/spreadsheets/d/%key%/gviz/tq?",keyFormat:new RegExp("spreadsheets/d/([^/#]+)","i"),gidFormat:new RegExp("gid=([^/&#]+)","i")},2010:{apiEndpoint:"https://spreadsheets.google.com/tq?key=%key%&",keyFormat:new RegExp("key=([^&#]+)","i"),gidFormat:new RegExp("gid=([^/&#]+)","i")}};var a={loaded:{},failed:{},labels:{},header:{},offset:{}};var n=0;var o=r.document;var i=typeof e!=="function";var s=r.jQuery&&r.jQuery.fn&&r.jQuery.fn.jquery;var f=false;if(!Array.prototype.forEach){Array.prototype.forEach=function(e){var r;var t=this;var a=t.length;for(r=0;r<a;r=r+1){e(t[r],r)}}}if(!Array.prototype.map){Array.prototype.map=function(e){var r=this;var t=[];r.forEach(function(r,a){t[a]=e(r,a)});return t}}if(!Object.keys){Object.keys=function(e){var r;var t=[];for(r in e){if(e.hasOwnProperty(r)){t.push(r)}}return t}}var u=function(e,r,t){if(!(e instanceof Error)){e=new Error(e)}if(r&&r.request&&r.request.index){a.failed[r.request.index]=true}if(r&&r.user&&r.user.callback){r.user.callback(e,r,t||null)}else{throw e}};var l=function(e){return e.toString().replace(/^ +/,"").replace(/ +$/,"")};var c=function(e){return Math.max(0,parseInt(e,10)||0)};var v=function(e,r){var t=" "+e.className+" ";return t.indexOf(" "+r+" ")!==-1};var d=function(e){e=e||{};if(e.jquery&&e.length){e=e[0]}return e.nodeType&&e.nodeType===1?e:false};var p=function(){if(o&&i){var e=o.createElement("script");f=e.addEventListener&&e.removeEventListener}return{dom:!!o,jquery:s?s:false,callbackOn404:i==="request"||f,transport:i?"jsonp":"request"}};var h=function(e){var r={};var a=Object.keys(t);a.forEach(function(a){var n=t[a];if(n.keyFormat.test(e)&&n.gidFormat.test(e)){r.key=e.match(n.keyFormat)[1];r.gid=e.match(n.gidFormat)[1];r.apiEndpoint=n.apiEndpoint.replace("%key%",r.key)}});return r};var y=function(e){return e.key&&e.gid?e.key+"_"+e.gid+"_"+e.query:null};var m=function(e){var r=e&&e.v?e.v:"";if(r instanceof Array){r=e.f||r.join("")}return l(r)};var b=function(e,r,t){var a={cells:{},cellsArray:r,labels:t,num:e};r.forEach(function(e,r){a.cells[t[r]]=e});return a};var g=function(e,r){return"<"+r+">"+e+"</"+r+">"};var q=function(e){var r=e.num?"td":"th";var t=Object.keys(e.cells);var a="";t.forEach(function(t){a+=g(e.cells[t],r)});return g(a,"tr")};var k=function(e){a.loaded[e]=false;a.failed[e]=false;a.labels[e]=false;a.header[e]=0;a.offset[e]=0};var E=function(e,r){var t={};var a=Object.keys(e);r.query=r.sql||r.query;r.reset=r.resetStatus||r.reset;r.fetchSize=r.chunkSize||r.fetchSize;r.rowTemplate=r.rowHandler||r.rowTemplate;a.forEach(function(a){t[a]=r[a]||e[a]});return t};var w=function(e,r){r.target=d(r.target)||d(e);r.fetchSize=c(r.fetchSize);return r};var x=function(e,r){var t=w(e,r);var n=h(t.url);n.query=t.query;n.index=y(n);if(t.reset&&n.index){k(n.index);n.reset=true}t.offset=a.offset[n.index]||0;if(t.fetchSize&&n.index){n.query+=" limit "+(t.fetchSize+1);n.query+=" offset "+t.offset;a.offset[n.index]=t.offset+t.fetchSize}return{user:t,request:n}};var j=function(e){if(!e.user.target&&!e.user.callback){throw"No element targeted or callback provided."}if(!(e.request.key&&e.request.gid)){throw"No key/gid in the provided URL."}if(a.failed[e.request.index]){throw"A previous request for this resource failed."}if(a.loaded[e.request.index]){throw"No more rows to load!"}return e};var O=function(e,r){return e&&e.length===r.length?e:r};var N=function(e,r){var t=e.request.index;var n=a.labels[t];var o=e.user.fetchSize;var i=r.table.rows;var s=r.table.cols;var f={last:i.length-1,rowNumberOffset:a.header[t]||0};if(!e.user.offset){n=s.map(function(e,r){if(e.label){return e.label.replace(/\s/g,"")}else{f.last=f.last+1;f.rowNumberOffset=1;return m(i[0].c[r]).replace(/\s/g,"")||e.id}});a.offset[t]=a.offset[t]+f.rowNumberOffset;a.header[t]=f.rowNumberOffset;a.labels[t]=n}if(!o||i.length-f.rowNumberOffset<o){f.last=f.last+1;a.loaded[t]=true}f.labels=O(e.user.labels,n);return f};var S=function(e,r,t){var a=[];var n=r.labels;if(!e.offset&&!r.rowNumberOffset){a.push(b(0,n,n))}t.table.rows.forEach(function(t,o){if(t.c&&o<r.last){var i=c(e.offset+o+1-r.rowNumberOffset);a.push(b(i,t.c.map(m),n))}});return a};var A=function(e,r,t){if(e.tagName==="TABLE"){var a=o.createElement("thead");var n=o.createElement("tbody");a.innerHTML=r;n.innerHTML=t;e.appendChild(a);e.appendChild(n)}else{e.insertAdjacentHTML("beforeEnd",r+t)}};var L=function(e,r){var t=e.rowTemplate||q;var a=o&&o.createElement&&e.target;var n=a&&e.target.tagName==="TABLE";var i=a&&v(e.target,"sheetrock-header");var s="";var f="";r.forEach(function(e){if(e.num){f+=t(e)}else if(n||i){s+=t(e)}});if(a){A(e.target,s,f)}return n?g(s,"thead")+g(f,"tbody"):s+f};var T=function(e,r){var t={raw:r};try{var a=t.attributes=N(e,r);var n=t.rows=S(e.user,a,r);t.html=L(e.user,n);if(e.user.callback){e.user.callback(null,e,t)}}catch(o){u("Unexpected API response format.",e,t)}};var z=function(r,t){var a={headers:{"X-DataSource-Auth":"true"},url:r.request.url};var n=function(e,a,n){if(!e&&a.statusCode===200){try{n=JSON.parse(n.replace(/^\)\]\}\'\n/,""));t(r,n)}catch(o){u(o,r,{raw:n})}}else{u(e||"Request failed.",r)}};e(a,n)};var R=function(e,t){var a=o.getElementsByTagName("head")[0];var i=o.createElement("script");var s="_sheetrock_callback_"+n;var l=function(){if(f){i.removeEventListener("error",v,false);i.removeEventListener("abort",v,false)}a.removeChild(i);delete r[s]};var c=function(r){try{t(e,r)}catch(a){u(a,e,{raw:r})}finally{l()}};var v=function(){u("Request failed.",e);l()};r[s]=c;e.request.url=e.request.url.replace("%callback%",s);if(f){i.addEventListener("error",v,false);i.addEventListener("abort",v,false)}i.type="text/javascript";i.src=e.request.url;a.appendChild(i);n=n+1};var F=function(e){var r=["gid="+encodeURIComponent(e.request.gid),"tq="+encodeURIComponent(e.request.query)];if(i){r.push("tqx=responseHandler:%callback%")}return e.request.apiEndpoint+r.join("&")};var C=function(e,r){e.request.url=F(e);if(i){R(e,r)}else{z(e,r)}};var H={url:"",query:"",target:null,fetchSize:0,labels:[],rowTemplate:null,callback:null,reset:false};var _=function(e,r){try{e=E(H,e||{});e=x(this,e);e=j(e);if(r){T(e,r)}else{C(e,T)}}catch(t){u(t,e)}return this};_.defaults=H;_.version="1.0.0";_.environment=p();if(s){r.jQuery.fn.sheetrock=_}return _});
//# sourceMappingURL=sheetrock.min.js.map

// cf https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API
function storageAvailable( type ) {
    try {
        var storage = window[type],
                x = '__storage_test__';
        storage.setItem( x, x );
        storage.removeItem( x );
        return true;
    }
    catch( e ) {
        return false;
    }
}


FloTools.fn.retrieveLocalStorage = function() {
    
    console.log('fn.retrieveLocalStorage...');
    
    if( storageAvailable('localStorage') ) {
        if( localStorage.FloToolsSheet ) {
            FloTools.sheet = localStorage.FloToolsSheet;
        }
        if( localStorage.FloToolsResponse ) {
            FloTools.response = JSON.parse( localStorage.FloToolsResponse );
        }
        if( localStorage.FloToolsState ) {
            FloTools.state = $.extend( FloTools.state, JSON.parse( localStorage.FloToolsState ) );
        }
    }
};

FloTools.fn.detectSheet = function() {
    
    console.log('fn.detectSheet...');
    
    FloTools.sheet = FloTools.sheet || '';
    
    if( !FloTools.sheet ) {
        FloTools.sheet = prompt("Quelle est la spreadsheet a utiliser ?");
        if( FloTools.sheet ) {
            localStorage.FloToolsSheet = FloTools.sheet;
        }
    }
};

FloTools.fn.loadSheet = function() {
    
    console.log('fn.loadSheet...');
    console.log( FloTools.sheet );
    
    if( !FloTools.sheet ) {
        console.warn('No sheet defined.');
        $log.text('Il faut d\'abord choisir une feuille de calcul.').show();
        return false;
    }
    
    var $table  = FloTools.container.find('table'),
        $log    = FloTools.container.find('.log'),
        $button = FloTools.container.find('.load-sheet');
    
    try {
        $log.text('Chargement de '+FloTools.sheet+'...').show();
        $table.empty();
        $button.addClass('disabled');
        sheetrock({
            url         : FloTools.sheet,
            query       : "SELECT * ",
            fetchSize   : 50,
            callback: function( error, options, response ) {
                console.log( error, options, response );
                FloTools.response = response;
                localStorage.FloToolsResponse = JSON.stringify( response );
                $log.text('').hide();
                $button.removeClass('disabled');
                if( error === null ) {
                    FloTools.fn.processResponse( response );
                }
            }
        });
    } catch( e ) {
        console.log(e);
        $log.text('Un problème est survenu lors du chargement de la spreadsheet.');
        $button.removeClass('disabled');
    }
};

FloTools.fn.processResponse = function( response ) {
    
    console.log('fn.processResponse...');
    
    var $table  = FloTools.container.find('table'),
        _rows   = response.rows,
        _len    = _rows.length,
        _thead  = '',
        _tbody  = '',
        _row, i, _tbodyLine;
    
    for( i = 0; i < _len; i++ ) {
        
        _row = _rows[i];
        
        if( i === 0 ) {
            _thead = '<th>' + _row.cellsArray.join('</th><th>') + '</th>';
        } else {
            _tbodyLine = '<td>' + _row.cellsArray.join('</td><td>') + '</td>';
            _tbody += '<tr data-index="' + i + '">' + _tbodyLine + '</tr>';
        }
        
    }
    
    _thead = '<thead><tr>' + _thead + '</tr></thead>';
    _tbody = '<tbody>' + _tbody + '</tbody>';
    
    $table.html( _thead + _tbody );
    
};

FloTools.fn.selectOption = function( $option ) {
    $option.siblings().removeAttr('selected');
    $option.attr('selected',true);
    $option.parent().prev().text( $option.text() );
};

FloTools.fn.handleClickSale = function( $tr ) {
    
    var _index      = $tr.data('index'),
        _row        = FloTools.response.rows[ _index ] || {},
        _data       = _row.cells || {},
        _country    = removeDiacritics( _data["Envoyéàunpays"] ).toLowerCase() || '----';

    console.log( _row );
    console.log( _data );
    
    FloTools.state.selectedRowIndex = _index;
    localStorage.FloToolsState = JSON.stringify( FloTools.state );
    
    $tr.siblings().removeClass('selected');
    $tr.addClass('selected');
    
    /*
     * Etape 1
     */
    $form = $('#dlbi-colis');
    if( $form.length > 0 ) {
        
        $form.find('#cp').val( '93100' );
        $form.find('#onlyPostalCode').val( '93100' );
        $form.find('#localite').val( 'MONTREUIL' )  ;


        FloTools.fn.selectOption( $('#destination option').filter(function () { return removeDiacritics( $(this).text().toLowerCase() ).search( _country ) !== -1; }).eq(0) );

        FloTools.fn.selectOption( $('#colistype option[value="2"]') );

        $form.find('#depot-bureau-poste1').attr('checked',true);
        $form.find('#colis-standard').attr('checked',true);

        $form.find('#indemnite-plus-non').attr('checked',true);

        $form.find('#poids').val( '0,7' );

        FloTools.fn.selectOption( $('#declaration-nature-colis option[value="cadeau"]') );

        // Douane
        $form.find('#desc-article-0').val( 'Paper sculpture in a glass bell' );
        $form.find('#poids-net-unitaire-0').val( '0,45' );
        $form.find('#quantite-0').val( '1' );
        $form.find('#valeur-unitaire-0').val( '20' );
        FloTools.fn.selectOption( $('#origin-country-0 option[value="fr"]') );

        FloTools.fn.selectOption( $('select[name="retourColis"] option[value="RETOUR_SANS_CHOIX"]') );
    }
    
    /*
     * Etape 2
     */
    
    $('#switchFormPart').click();
    
    $form = $('#coordonneesParticulier');
    
    if( $form.length > 0 ) {
        
        $form.find('#civiliteMr').attr('checked',true);
        
        $form.find('#lastName').val( _data['Nom'] );
        $form.find('#firstName').val( _data['Prénom'] );
        $form.find('#batiment').val( _data['Rue2'] );
        $form.find('#numLibelle').val( _data['Rue1'] );
        $form.find('#lieuDit').val( _data['Envoyéàunétat'] );
        $form.find('#codePostal').val( _data['Envoyéàuncodepostal'] );
        $form.find('#localite').val( _data['Envoyéàuneville'] );
        
        FloTools.state.selectedRowIndex = undefined;
        localStorage.FloToolsState = JSON.stringify( FloTools.state );
    }

};

/*
 * Initialisation
 */
(function(){
    
    /*if( location.host !== "pro.boutique.laposte.fr" ) {
        alert( 'Domaine non pris en compte' );
        return;
    }*/
    
    /*
     * Vérification que la page est OK
     */
    /*switch( location.pathname ) {
        default:
            FloTools.type = undefined;
            break;
    }*/   
    
    /*if( !FloTools.type ) {
        alert( 'Please go to a LinkedIn Search (Companies or People) page first :)' );
        return;
    }*/
    
    /*
     * UI
     */
    if( $( FloTools.containerId ).length === 0 ) {
        $('body').append(
            '<div id="' + FloTools.containerId.replace('#','') + '"> \
                <p class="log"></p>\
                <table id="flo-table" class="table table-condensed table-striped"></table>\
                <div id="flo-filter">\
                    <input type="text" id="flo-search" placeholder="Filtrer les commandes">\
                </div> \
                <div id="flo-actions">\
                    <button class="flo-btn load-sheet">Recharger</button>\
                </div> \
            </div> \
            <style>' +
                FloTools.containerId + ' { \
                    background: #fff; \
                    border-top: 2px solid orange; \
                    bottom: 0; \
                    box-sizing: border-box; \
                    font-size: 16px; \
                    height: 200px; \
                    left: 0; \
                    padding: 20px; \
                    overflow: auto; \
                    position: fixed; \
                    right: 0; \
                    z-index: 10000000; \
                } \
                .log { display: none; } \
                #flo-search { \
                    height: 30px; \
                    padding: 5px 10px; \
                    font-size: 12px; \
                    line-height: 1.5; \
                    border-radius: 3px; \
                    color: #555; \
                    background-color: #fff; \
                    background-image: none; \
                    border: 1px solid orange; \
                    border-radius: 4px; \
                } \
                #flo-search:focus { \
                    border-color: #66afe9; \
                    outline: 0; \
                    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6); \
                    box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6); \
                } \
                #flo-filter { \
                    bottom: 0; \
                    padding: 5px; \
                    position: fixed; \
                    left: 0; \
                } \
                #flo-actions { \
                    bottom: 0; \
                    padding: 5px; \
                    position: fixed; \
                    right: 0; \
                } \
                .flo-btn { \
                    background-color: #fff; \
                    border: 1px solid orange; \
                    color: orange; \
                    display: inline-block; \
                    padding: 6px 12px; \
                    margin-bottom: 0; \
                    font-size: 12px; \
                    font-weight: 400; \
                    line-height: 1.42857143; \
                    text-align: center; \
                    white-space: nowrap; \
                    vertical-align: middle; \
                    -ms-touch-action: manipulation; \
                    touch-action: manipulation; \
                    cursor: pointer; \
                    -webkit-user-select: none; \
                    -moz-user-select: none; \
                    -ms-user-select: none; \
                    user-select: none; \
                    background-image: none; \
                    border-radius: 4px; \
                } \
                .flo-btn.disabled { opacity: 0.5; } \
                #flo-table th { font-size: 12px; padding: 0px 10px 5px 0; } \
                #flo-table td { padding: 0px 10px 0 0; } \
                #flo-table tbody tr.selected, #flo-table tbody tr.selected td { background: #FFCB6B !important; color: #fff !important; } \
                #flo-table tbody tr:hover, #flo-table tbody tr:hover td { background: orange !important; color: #fff !important; cursor: pointer; } \
            </style>'
        );
    }

    /*
     * Initialisation
     */
    FloTools.container     = $( FloTools.containerId );
    FloTools.getParams     = FloTools_Utilities.extractGetParams();
    
    FloTools.fn.retrieveLocalStorage();
    
    FloTools.fn.detectSheet();

    if( FloTools.response ) {
        FloTools.fn.processResponse( FloTools.response );
        
        if( FloTools.state.selectedRowIndex !== undefined ) {
            console.log('Auto-selecting row #'+FloTools.state.selectedRowIndex);
            $tr = FloTools.container.find('table tbody tr[data-index="' + FloTools.state.selectedRowIndex + '"]');
            console.log( $tr );
            FloTools.fn.handleClickSale( $tr );
        }
        
    } else {
        FloTools.fn.loadSheet();
    }
    
    // Events
    FloTools.container.find('.load-sheet').click( FloTools.fn.loadSheet );    
    FloTools.container.find('table').on( 'click', 'tbody tr', function() { FloTools.fn.handleClickSale( $(this) ); } );
    
    // cf jsfiddle.net/7BUmG
    $('#flo-search').keyup(function() {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $('#flo-table tbody tr').show().filter(function() {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });

})();


